package nl.navdeep.targetacquired.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import nl.navdeep.targetacquired.dto.AccountCredentialDTO;
import nl.navdeep.targetacquired.dto.AccountDTO;
import nl.navdeep.targetacquired.dto.RoutesDTO;
import nl.navdeep.targetacquired.exception.ObjectNotFoundException;
import nl.navdeep.targetacquired.model.Account;
import nl.navdeep.targetacquired.model.AccountCredential;
import nl.navdeep.targetacquired.model.Routes;
import nl.navdeep.targetacquired.services.IAccountCredentialService;
import nl.navdeep.targetacquired.services.IAccountService;
import nl.navdeep.targetacquired.services.IRoutesService;

@RestController
public class ControllerAccount {

	@Autowired
	private IAccountService iAccountService;

	@Autowired
	private IAccountCredentialService iAccountCredentialService;

	@Autowired
	private IRoutesService iRoutesService;
	
	
	@GetMapping("/api/account/{account_id:[0-9]+}")
	public AccountDTO findOne(@PathVariable("account_id") Long id) {
		Optional<Account> accountOpt = this.iAccountService.findOne(id);
		Optional<AccountCredential> accountCredentialOpt = this.iAccountCredentialService.findOne(id);
		Optional<Routes> routesOpt = this.iRoutesService.findOne(id);

		if (accountOpt.isPresent())
			return AccountDTO.buildFromAccount(accountOpt.get(), accountCredentialOpt.get(), routesOpt.get());

		throw new ObjectNotFoundException();
	}

	
	@PostMapping("/api/account/create")
	public boolean create( @RequestBody AccountCredentialDTO accountCredentialDTO, RoutesDTO routesDTO ) {
		Account account = new Account();
		AccountCredential accountCredential = new AccountCredential();
		Routes route = new Routes();
		
		account.setAccount_Id(accountCredentialDTO.getAccount_credential_id());
		account.setFirstName(accountCredentialDTO.getFirstName());
		account.setLastName(accountCredentialDTO.getLastName());
		account.setAge(accountCredentialDTO.getAge());
		account.setCity(accountCredentialDTO.getCity());
		account.setStreetName(accountCredentialDTO.getStreetName());
		account.setPostalCode(accountCredentialDTO.getPostalCode());
		account.setHousNr(accountCredentialDTO.getHousNr());
		account.setEmail(accountCredentialDTO.getEmail());

		
		accountCredential.setUserName(accountCredentialDTO.getUserName());
		accountCredential.setPassword(accountCredentialDTO.getPassword());
		
		route.setRouteName(routesDTO.getRouteName());
		
		account = this.iAccountService.register(account, accountCredential, route);
		System.out.println("Created account with id " + account.getAccount_Id());

		return true;
	}
/*
	@PostMapping("/api/account/{account_id:[0-9]+}")
	public String createNewPerson(HttpServletRequest request) {

		// lees formulier uit
		Account account = new Account();
		AccountCredential accountCredential = new AccountCredential();
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String ageAsString = request.getParameter("age");
		String userName = request.getParameter("userName");
		String eMail = request.getParameter("eMail");

		// maakt een persoon

		account.setFirstName(firstName);
		account.setLastName(lastName);
		account.setAge(Integer.parseInt(ageAsString));
		account.setEmail(eMail);

		accountCredential.setUserName(userName);
		accountCredential.setAccount(account);

		// slaat gebruiker op
		this.iAccountService.create(account);
		this.iAccountCredentialService.create(accountCredential);

		return "redirect:/form";
	} */
}

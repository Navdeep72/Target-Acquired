package nl.navdeep.targetacquired.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import nl.navdeep.targetacquired.dto.AccountCredentialDTO;
import nl.navdeep.targetacquired.exception.ObjectNotFoundException;
import nl.navdeep.targetacquired.model.AccountCredential;
import nl.navdeep.targetacquired.services.IAccountCredentialService;

@RestController
public class ControllerAccountCredential {

	@Autowired
	private IAccountCredentialService iAccountCredentialService;

	@GetMapping("/api/accountcredential/{account_credential_id:[0-9]+}")
	public AccountCredentialDTO findOne1(@PathVariable("account_credential_id") Long id) {
		Optional<AccountCredential> account_credentialOpt = this.iAccountCredentialService.findOne(id);

		if (account_credentialOpt.isPresent())
			return AccountCredentialDTO.buildFromAccountCredential(account_credentialOpt.get());

		throw new ObjectNotFoundException();
	}
	
/*
	@PostMapping("/api/accountcredential/{account_credential_id:[0-9]+}")
	public String createNewSight(HttpServletRequest request) {

		AccountCredential account_credential = new AccountCredential();
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");

		account_credential.setUserName(userName);
		account_credential.setPassword(password);

		this.iAccountCredentialService.create(account_credential);

		return "redirect:/form";
	}*/
}

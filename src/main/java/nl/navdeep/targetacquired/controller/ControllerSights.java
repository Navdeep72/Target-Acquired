package nl.navdeep.targetacquired.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import nl.navdeep.targetacquired.dto.SightsDTO;
import nl.navdeep.targetacquired.exception.ObjectNotFoundException;
import nl.navdeep.targetacquired.model.Sights;
import nl.navdeep.targetacquired.services.ISightsService;

@RestController
public class ControllerSights {

	@Autowired
	private ISightsService iSightsService;

		
	@GetMapping("/api/sights/all")
	public List<SightsDTO> findAll() {
		return this.iSightsService.findAll()
				.stream().map(u -> SightsDTO.buildFromSights(u))
				.collect(Collectors.toList());
	}
	
	  @GetMapping("/api/sights/{sight_id:[0-9]+}") 
	  public SightsDTO findOne(@PathVariable("sight_id") Long id) {
			Optional<Sights> sightOpt = this.iSightsService.findOne(id);

			if (sightOpt.isPresent())
				return SightsDTO.buildFromSights(sightOpt.get());

			throw new ObjectNotFoundException();
	 
	}
}

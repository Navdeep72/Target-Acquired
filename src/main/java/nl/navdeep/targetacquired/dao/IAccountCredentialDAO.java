package nl.navdeep.targetacquired.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import nl.navdeep.targetacquired.model.AccountCredential;

public interface IAccountCredentialDAO extends PagingAndSortingRepository<AccountCredential, Long>{
	
	Page<AccountCredential> findAll(Pageable pageable);
	
}

package nl.navdeep.targetacquired.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import nl.navdeep.targetacquired.model.Account;

public interface IAccountDAO extends PagingAndSortingRepository<Account, Long>{
	
	Page<Account> findAll(Pageable pageable);
	

}

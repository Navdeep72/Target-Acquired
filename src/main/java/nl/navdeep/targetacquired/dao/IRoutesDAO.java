package nl.navdeep.targetacquired.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import nl.navdeep.targetacquired.model.Routes;

public interface IRoutesDAO extends PagingAndSortingRepository<Routes, Long>{
	
	Page<Routes> findAll(Pageable pageable);

}

package nl.navdeep.targetacquired.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import nl.navdeep.targetacquired.model.Route_points;


public interface IRoutes_pointsDAO extends PagingAndSortingRepository<Route_points, Long>{
	
	Page<Route_points> findAll(Pageable pageable);

}

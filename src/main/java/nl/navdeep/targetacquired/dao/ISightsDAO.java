package nl.navdeep.targetacquired.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import nl.navdeep.targetacquired.model.Sights;


	public interface ISightsDAO extends PagingAndSortingRepository<Sights, Long>{
		
		List<Sights> findAll();
}

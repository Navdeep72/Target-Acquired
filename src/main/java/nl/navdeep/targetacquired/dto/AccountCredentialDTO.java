package nl.navdeep.targetacquired.dto;

import nl.navdeep.targetacquired.model.AccountCredential;

public class AccountCredentialDTO extends AccountDTO{

	private Long account_credential_id;

	private String userName;

	private String password;

	public AccountCredentialDTO() {

	}

	public AccountCredentialDTO(Long account_credential_id, String userName, String password) {
		this.account_credential_id = account_credential_id;
		this.userName = userName;
		this.password = password;

	}

	public Long getAccount_credential_id() {
		return account_credential_id;
	}

	public void setAccount_credential_id(Long account_credential_id) {
		this.account_credential_id = account_credential_id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static final AccountCredentialDTO buildFromAccountCredential(AccountCredential account_credential) {
		return new AccountCredentialDTO(account_credential.getAccount_Credential_Id(),
				account_credential.getUserName(), account_credential.getPassword());
	}

}

package nl.navdeep.targetacquired.dto;

import nl.navdeep.targetacquired.model.Account;
import nl.navdeep.targetacquired.model.AccountCredential;
import nl.navdeep.targetacquired.model.Routes;

public class AccountDTO {
	
	private Long account_id;
	
	private String firstName;
	
	private String lastName;
	
	private int age;
	
	private String userName;
	
	private String routeName;
	
	private String streetName;
	
	private String housNr;
	
	private String postalCode;
	
	private String city;

	private String email;

	public AccountDTO() {
		
	}

	public AccountDTO(Long account_id, String firstName, String lastName, int age, String userName, String routeName, String streetName, String housNr, String postalCode, String city, String email) {
		this.account_id = account_id;
		this.firstName = firstName;
		this.age = age;
		this.lastName = lastName;
		this.userName = userName;
		this.routeName = routeName;
		this.streetName = streetName;
		this.housNr = housNr;
		this.postalCode = postalCode;
		this.city = city;
		this.email = email;
	}


	public Long getAccount_Id() {
		return account_id;
	}

	public void setAccount_Id(Long id) {
		this.account_id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getHousNr() {
		return housNr;
	}

	public void setHousNr(String housNr) {
		this.housNr = housNr;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static final AccountDTO buildFromAccount(Account account, AccountCredential accountCredential, Routes routes) {
		return new AccountDTO(account.getAccount_Id(), account.getFirstName(), account.getLastName(), account.getAge(),accountCredential.getUserName(), routes.getRouteName(), account.getStreetName(), account.getHousNr()
				, account.getPostalCode(), account.getCity(), account.getEmail());
	}

}

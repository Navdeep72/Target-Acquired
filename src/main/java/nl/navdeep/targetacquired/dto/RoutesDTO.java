package nl.navdeep.targetacquired.dto;

import javax.persistence.ManyToOne;

import nl.navdeep.targetacquired.model.Account;
import nl.navdeep.targetacquired.model.Routes;

public class RoutesDTO {
	
	private Long route_id;
	
	private String routeName;
	
	@ManyToOne
	private Account account;

	public RoutesDTO() {}
	
	public RoutesDTO(Long route_id, String routeName) {
		this.route_id = route_id;
		this.routeName = routeName;
	}

	public Long getRoute_id() {
		return route_id;
	}

	public void setRoute_id(Long route_id) {
		this.route_id = route_id;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
	public static final RoutesDTO buildFromAccount(Routes routes) {
		return new RoutesDTO(routes.getRoute_Id(), routes.getRouteName());
	}
}

package nl.navdeep.targetacquired.dto;


import nl.navdeep.targetacquired.model.Sights;

public class SightsDTO {

	private Long sight_id;	
	
	private String sight_name;
	
	private String category;
	
	private double lat;
	
	private double lng;
	
	public SightsDTO(){
		
	}
	
	public SightsDTO(long sights_id, String sight_name, String category, double lat, double lng) {
		this.sight_id = sights_id;
		this.sight_name = sight_name;
		this.category = category;
		this.lat = lat;
		this.lng = lng;
				
	}
	
	public Long getSight_id() {
		return sight_id;
	}

	public void setSight_id(Long sights_id) {
		this.sight_id = sights_id;
	}

	public String getSight_name() {
		return sight_name;
	}

	public void setSight_name(String sights_name) {
		this.sight_name = sights_name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public static final SightsDTO buildFromSights(Sights sights) {
		return new SightsDTO(sights.getSight_id(), sights.getSightName(), sights.getCategory(), sights.getLat(), sights.getLng());
	}
	
}

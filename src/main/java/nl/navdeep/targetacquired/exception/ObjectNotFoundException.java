package nl.navdeep.targetacquired.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ObjectNotFoundException extends RuntimeException{

	private static final long serialVersionUID = -4411437465291503801L;

}

package nl.navdeep.targetacquired.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Account {
	
	@Id
	@GeneratedValue
	private Long account_id;
	
	private String firstName;
	
	private String lastName;
	
	private String streetName;
	
	private String housNr;
	
	private String postalCode;
	
	private String city;

	@Column(nullable=false)
	private int age;
	
	private String email;
	
	@OneToOne(mappedBy = "account")
	private AccountCredential accountCredentials;
	
	@OneToMany
	private List<Routes> routes;
	
// getters and setters
// id
	public Long getAccount_Id() {
		return account_id;
	}

	public void setAccount_Id(Long account_id) {
		this.account_id = account_id;
	}
// firstname	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
// lastname
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
// streetname
	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
// housnr
	public String getHousNr() {
		return housNr;
	}

	public void setHousNr(String housNr) {
		this.housNr = housNr;
	}
// postalcode
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
// city
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
// age
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
// email
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
// account cred
	public AccountCredential getAccountCredentials() {
		return accountCredentials;
	}

	public void setAccountCredentials(AccountCredential accountCredentials) {
		this.accountCredentials = accountCredentials;
	}
// route
	public List<Routes> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Routes> routes) {
		this.routes = routes;
	}

}

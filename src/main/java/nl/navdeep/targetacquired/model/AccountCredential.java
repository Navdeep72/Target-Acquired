package nl.navdeep.targetacquired.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;



@Entity
public class AccountCredential{
	
	@Id
	@GeneratedValue
	private Long account_Credential_Id;
	
	@Column
	private String userName;

	@Column
	private String password;
	
	@OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name = "account_id")
	private Account account;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public AccountCredential() {}
	
	public AccountCredential(Long acoount_Credential_Id, String userName, String password) {
		setAccount_Crendential_Id(account_Credential_Id);
		setUserName(userName);
		setPassword(password);
		
		getAccount_Credential_Id();
		getUserName();
		getPassword();
		
	}


	public Long getAccount_Credential_Id() {
		return account_Credential_Id;
	}

	public void setAccount_Crendential_Id(Long account_Credential_Id) {
		this.account_Credential_Id = account_Credential_Id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

package nl.navdeep.targetacquired.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Route_points {
	

	@Id
	@GeneratedValue
	private Long route_points_id;
	
	private double lat;
	
	private double lng;

	
	public Long getRoute_Points_Id() {
		return route_points_id;
	}

	public void setRoute_Points_Id(Long id) {
		this.route_points_id = id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}
}


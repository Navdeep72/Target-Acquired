package nl.navdeep.targetacquired.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Routes {

	@Id
	@GeneratedValue
	private Long route_id;
	
	private String routeName;
	
	@OneToMany()
	private List <Route_points> route_points;
	
	@ManyToMany
	private List<Sights> sights;
	
	@ManyToOne
	private Account account;
	
// getter setters
// id
	public Long getRoute_Id() {
		return route_id;
	}

	public void setRoute_Id(Long id) {
		this.route_id = id;
	}
// route name
	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
// route points
	public List<Route_points> getRoute_points() {
		return route_points;
	}

	public void setRoute_points(List<Route_points> route_points) {
		this.route_points = route_points;
	}
// sights
	public List<Sights> getSights() {
		return sights;
	}
	public void setSights(List<Sights> sights) {
		this.sights = sights;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Long getRoute_id() {
		return route_id;
	}

	public void setRoute_id(Long route_id) {
		this.route_id = route_id;
	}
	
	
	
}

package nl.navdeep.targetacquired.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Sights {

	@Id
	@GeneratedValue
	private Long sight_id;
	
	private String sightName;
	
	private String category;
	
	private double lat;
	
		private double lng;
	
	@ManyToMany()
	private List <Route_points> route_points;
	
	public List<Route_points> getRoute_points() {
		return route_points;
	}

	public void setRoute_Point(List<Route_points> route_points) {
		this.route_points = route_points;
	}
	
	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}


	public Long getSight_id() {
		return sight_id;
	}

	public void setSight_Id(Long id) {
		this.sight_id = id;
	}

	public String getSightName() {
		return sightName;
	}

	public void setSightName(String sightName) {
		this.sightName = sightName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
		
}

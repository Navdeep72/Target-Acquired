package nl.navdeep.targetacquired.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import nl.navdeep.targetacquired.dao.IAccountCredentialDAO;
import nl.navdeep.targetacquired.model.AccountCredential;

@Service
public class AccountCredentialService implements IAccountCredentialService{
	
	@Autowired
	private IAccountCredentialDAO iAccountCredentialDAO;

	@Override
	public Page<AccountCredential> findAll(int page, int size, Sort sort) {
		Pageable pageable = new PageRequest(page, size, sort);
		
		return this.iAccountCredentialDAO.findAll(pageable);
	}

	@Override
	public AccountCredential create(AccountCredential accountCredential) {
		
		return this.iAccountCredentialDAO.save(accountCredential);
	}

	@Override
	public Optional<AccountCredential> findOne(Long id) {
		
		return Optional.ofNullable(iAccountCredentialDAO.findOne(id));
	}

	@Override
	public void update(AccountCredential accountCredential) {
		
		this.iAccountCredentialDAO.save(accountCredential);
		
	}

	@Override
	public void delete(AccountCredential accountCredential) {
		this.iAccountCredentialDAO.delete(accountCredential);
		
	}


}

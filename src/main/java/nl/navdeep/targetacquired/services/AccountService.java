package nl.navdeep.targetacquired.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import nl.navdeep.targetacquired.dao.IAccountCredentialDAO;
import nl.navdeep.targetacquired.dao.IAccountDAO;
import nl.navdeep.targetacquired.dao.IRoutesDAO;
import nl.navdeep.targetacquired.model.Account;
import nl.navdeep.targetacquired.model.AccountCredential;
import nl.navdeep.targetacquired.model.Routes;

@Service
@Transactional
public class AccountService implements IAccountService{
	
	@Autowired
	private IAccountDAO iAccountDAO;
	
	@Autowired
	private IAccountCredentialDAO iAccountCredentialDAO;
	
	@Autowired
	private IRoutesDAO iRouteDAO;

	@Override
	public Account create(Account account) {
		return this.iAccountDAO.save(account);
	}

	@Override
	public Page<Account> findAll(int page, int size, Sort sort) {
		Pageable pageable = new PageRequest(page, size, sort);
		
		return this.iAccountDAO.findAll(pageable);
	}

	@Override
	public Optional<Account> findOne(Long id) {
		return Optional.ofNullable(iAccountDAO.findOne(id));
	}

	@Override
	public void update(Account account) {
		this.iAccountDAO.save(account);
		
	}

	@Override
	public void delete(Account account) {
		this.iAccountDAO.delete(account);
	}

	@Override
	public Account register(Account account, AccountCredential accountCredential, Routes route) {
		account = this.iAccountDAO.save(account);
		accountCredential.setAccount(account);
		route.setAccount(account);
		
		this.iAccountCredentialDAO.save(accountCredential);
		this.iRouteDAO.save(route);
		return account;
	}

}

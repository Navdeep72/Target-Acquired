package nl.navdeep.targetacquired.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import nl.navdeep.targetacquired.model.AccountCredential;

public interface IAccountCredentialService {
	
	Page<AccountCredential> findAll(int page, int size, Sort sort);
	
	AccountCredential create(AccountCredential accountCredential);
	
	Optional<AccountCredential> findOne( Long id );
	
	void update(AccountCredential accountCredential);

	void delete(AccountCredential accountCredential);


}

package nl.navdeep.targetacquired.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import nl.navdeep.targetacquired.model.Account;
import nl.navdeep.targetacquired.model.AccountCredential;
import nl.navdeep.targetacquired.model.Routes;

public interface IAccountService {
	
	Page<Account> findAll(int page, int size, Sort sort);
	
	Account create(Account account);
	
	Optional<Account> findOne( Long id );
	
	void update(Account account);

	void delete(Account account);
	
	Account register(Account account, AccountCredential accountCredentiald, Routes route);
	

}

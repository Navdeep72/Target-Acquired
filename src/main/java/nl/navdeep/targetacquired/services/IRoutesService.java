package nl.navdeep.targetacquired.services;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import nl.navdeep.targetacquired.model.Routes;

public interface IRoutesService {
	
	Page<Routes> findAll(int page, int size, Sort sort);
	
	Routes create(Routes routes);
	
	Optional<Routes> findOne( Long id );
	
	void update(Routes routes);

	void delete(Routes routes);

}

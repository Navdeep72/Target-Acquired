package nl.navdeep.targetacquired.services;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import nl.navdeep.targetacquired.model.Route_points;

public interface IRoutes_pointsService {
	
	Page<Route_points> findAll(int page, int size, Sort sort);
	
	Route_points create(Route_points routePoints);
	
	Optional<Route_points> findOne( Long id );
	
	void update(Route_points routePoints);

	void delete(Route_points routePoints);

}

package nl.navdeep.targetacquired.services;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import nl.navdeep.targetacquired.model.Sights;

public interface ISightsService {

	
	List<Sights> findAll();
	
	Sights create(Sights routes);
	
	Optional<Sights> findOne( Long id );
	
	void update(Sights routes);

	void delete(Sights routes);

}

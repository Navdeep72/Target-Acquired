package nl.navdeep.targetacquired.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import nl.navdeep.targetacquired.dao.IRoutesDAO;
import nl.navdeep.targetacquired.model.Routes;

@Service
@Transactional
public class RoutesService implements IRoutesService{
	
	@Autowired
	private IRoutesDAO iRoutesDAO;

	@Override
	public Page<Routes> findAll(int page, int size, Sort sort) {
		Pageable pageable = new PageRequest(page, size, sort);
		
		return this.iRoutesDAO.findAll(pageable);
	}

	@Override
	public Routes create(Routes routes) {
		return this.iRoutesDAO.save(routes);
	}

	@Override
	public Optional<Routes> findOne(Long id) {
		return Optional.ofNullable(iRoutesDAO.findOne(id));
	}

	@Override
	public void update(Routes routes) {
		this.iRoutesDAO.save(routes);
		
	}

	@Override
	public void delete(Routes routes) {
		this.iRoutesDAO.delete(routes);
	}

}

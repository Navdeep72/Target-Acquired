package nl.navdeep.targetacquired.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import nl.navdeep.targetacquired.dao.IRoutes_pointsDAO;
import nl.navdeep.targetacquired.model.Route_points;

@Service
@Transactional
public class Routes_pointsService implements IRoutes_pointsService{
	
	@Autowired
	private IRoutes_pointsDAO iRoutes_pointsDAO;

	@Override
	public Page<Route_points> findAll(int page, int size, Sort sort) {
		Pageable pageable = new PageRequest(page, size, sort);
		
		return this.iRoutes_pointsDAO.findAll(pageable);
	}

	@Override
	public Route_points create(Route_points routePoints) {
		return this.iRoutes_pointsDAO.save(routePoints);
	}

	@Override
	public Optional<Route_points> findOne(Long id) {
		return Optional.ofNullable(iRoutes_pointsDAO.findOne(id));
	}

	@Override
	public void update(Route_points routePoints) {
		this.iRoutes_pointsDAO.save(routePoints);
	}

	@Override
	public void delete(Route_points routePoints) {
		this.iRoutes_pointsDAO.delete(routePoints);
		
	}

}

package nl.navdeep.targetacquired.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import nl.navdeep.targetacquired.dao.ISightsDAO;
import nl.navdeep.targetacquired.model.Sights;

@Service
@Transactional
public class SightsService implements ISightsService{
	
	@Autowired
	private ISightsDAO iSightsDAO;	

	@Override
	public Sights create(Sights sights) {
		return this.iSightsDAO.save(sights);
	}

	@Override
	public Optional<Sights> findOne(Long id) {
		return Optional.ofNullable(iSightsDAO.findOne(id));
	}

	@Override
	public void update(Sights sights) {
		this.iSightsDAO.save(sights);
		
	}

	@Override
	public void delete(Sights sights) {
		this.iSightsDAO.delete(sights);
	}

	@Override
	@Transactional
	public List<Sights> findAll() {
		return (List<Sights>) this.iSightsDAO.findAll();
	}
}

